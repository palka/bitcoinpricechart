package com.nekodev.pricechart.presentation.chart.axis

import android.content.res.Resources
import com.github.mikephil.charting.components.AxisBase
import com.nekodev.pricechart.R
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.impl.annotations.MockK
import io.mockk.verify
import org.amshove.kluent.`should be equal to`
import org.junit.Before
import org.junit.Test

class PriceAxisFormatterTest {

    @MockK
    lateinit var axisBase: AxisBase

    @MockK
    lateinit var resources: Resources

    @InjectMockKs
    lateinit var formatter: PriceAxisFormatter

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxUnitFun = true)
    }

    @Test
    fun `GIVEN value WHEN getFormattedValue THEN rounded up value passed AND result from resources returned`() {
       checkCorrectValuePassedToResourcesAndExpectedResultReturned(135.7, 136)
    }

    @Test
    fun `GIVEN value WHEN getFormattedValue THEN rounded down value passed AND result from resources returned`() {
        checkCorrectValuePassedToResourcesAndExpectedResultReturned(76.2, 76)
    }

    private fun checkCorrectValuePassedToResourcesAndExpectedResultReturned(passedPrice: Double, expectedPrice: Int) {
        //given
        val value = passedPrice.toFloat()
        val expectedResult = "someResult"
        every { resources.getString(any(), any()) } returns expectedResult

        //when
        val result = formatter.getFormattedValue(value, axisBase)

        //then
        verify{
            resources.getString(R.string.axis_price_format, expectedPrice)
        }
        result `should be equal to` expectedResult
    }
}
package com.nekodev.pricechart.presentation.chart.axis

import com.nekodev.pricechart.data.TimeSpan
import org.amshove.kluent.`should equal`
import org.junit.Test

class DateFormatterFactoryTest {

    private val factory = DateFormatterFactory()

    @Test
    fun `GIVEN time span 30 days WHEN createDateFormatter THEN formatter with pattern dd MMM`() {
        checkPatternForTimeSpan(TimeSpan.DAYS_30, "dd MMM")
    }

    @Test
    fun `GIVEN time span 60 days WHEN createDateFormatter THEN formatter with pattern dd MMM`() {
        checkPatternForTimeSpan(TimeSpan.DAYS_60, "dd MMM")
    }

    @Test
    fun `GIVEN time span 180 days WHEN createDateFormatter THEN formatter with pattern MMM`() {
        checkPatternForTimeSpan(TimeSpan.DAYS_180, "MMM")
    }

    @Test
    fun `GIVEN time span year WHEN createDateFormatter THEN formatter with pattern MMM`() {
        checkPatternForTimeSpan(TimeSpan.ONE_YEAR, "MMM")
    }

    @Test
    fun `GIVEN time span 2 years WHEN createDateFormatter THEN formatter with pattern MMM YY`() {
        checkPatternForTimeSpan(TimeSpan.TWO_YEARS, "MMM YYYY")
    }

    @Test
    fun `GIVEN time span all WHEN createDateFormatter THEN formatter with pattern YYYY`() {
        checkPatternForTimeSpan(TimeSpan.ALL_TIME, "YYYY")
    }

    private fun checkPatternForTimeSpan(timeSpan: TimeSpan, expectedPattern: String) {
        val result = factory.createDateFormatterForTimeSpan(timeSpan)
        result.toPattern() `should equal` expectedPattern
    }
}
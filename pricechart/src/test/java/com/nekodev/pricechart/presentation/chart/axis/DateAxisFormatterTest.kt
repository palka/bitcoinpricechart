package com.nekodev.pricechart.presentation.chart.axis

import com.github.mikephil.charting.components.AxisBase
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.impl.annotations.MockK
import io.mockk.verify
import org.amshove.kluent.`should be equal to`
import org.junit.Before
import org.junit.Test
import java.text.SimpleDateFormat

class DateAxisFormatterTest {

    @MockK
    lateinit var axisBase: AxisBase

    @MockK
    lateinit var dateFormatter: SimpleDateFormat

    @InjectMockKs
    lateinit var formatter: DateAxisFormatter

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxUnitFun = true)
    }

    @Test
    fun `GIVEN value WHEN getFormattedValue THEN rounded up value passed AND result returned`() {
        checkCorrectValuePassedToFormatterAndExpectedResultReturned(123447.91f, 123448)
    }

    @Test
    fun `GIVEN value WHEN getFormattedValue THEN rounded down value passed AND result returned`() {
        checkCorrectValuePassedToFormatterAndExpectedResultReturned(75.2f, 75)
    }

    private fun checkCorrectValuePassedToFormatterAndExpectedResultReturned(value: Float, expectedTimestamp: Long) {
        //given
        val expectedResult = "myResult"
        every { dateFormatter.format(any<Long>()) } returns expectedResult

        //when
        val result = formatter.getFormattedValue(value, axisBase)

        //then
        verify {
            dateFormatter.format(expectedTimestamp)
        }
        result `should be equal to` expectedResult
    }
}
package com.nekodev.pricechart.presentation

import com.github.mikephil.charting.data.Entry
import com.nekodev.base.util.rx.ImmediateSchedulerProvider
import com.nekodev.pricechart.data.MarketPriceRepository
import com.nekodev.pricechart.data.PriceToEntryMapper
import com.nekodev.pricechart.data.TimeSpan
import com.nekodev.pricechart.data.dao.MarketPriceInTime
import com.nekodev.pricechart.data.dao.PricesWithTimeSpan
import io.mockk.*
import io.mockk.impl.annotations.MockK
import io.reactivex.Observable
import io.reactivex.Single
import org.amshove.kluent.`should equal`
import org.junit.Before
import org.junit.Test

class PriceChartPresenterTest {

    @MockK(relaxed = true)
    lateinit var view: PriceChartView

    @MockK
    lateinit var repository: MarketPriceRepository

    @MockK
    lateinit var entryMapper: PriceToEntryMapper

    private lateinit var presenter: PriceChartPresenter

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxUnitFun = true)
        presenter = PriceChartPresenter(
                ImmediateSchedulerProvider(),
                repository,
                entryMapper
        )

        val mockEntry = mockk<Entry>()
        every { entryMapper.invoke(any()) } returns mockEntry
    }

    @Test
    fun `GIVEN prices requested AND fetch prices error WHEN register THEN show progress then show error`() {
        //given
        every { view.onPricesRequested() } returns Observable.just(TimeSpan.DAYS_180)
        every { repository.fetchMarketPrices(any()) } returns Single.error(Throwable())

        //when
        presenter.register(view)

        //then
        verifyOrder {
            view.showLoader()
            view.showError()
        }
    }

    @Test
    fun `GIVEN prices requested AND fetch prices success WHEN register THEN show progress then show current price and price chart`() {
        //given
        val priceInTime1 = MarketPriceInTime(111, 222.1)
        val priceInTime2 = MarketPriceInTime(11334, 1222.5)
        val priceInTime3 = MarketPriceInTime(99999, 5552.99)
        val timeSpan = TimeSpan.DAYS_180

        val fetchedPrices = PricesWithTimeSpan(timeSpan, listOf(priceInTime1, priceInTime2, priceInTime3))
        every { view.onPricesRequested() } returns Observable.just(timeSpan)
        every { repository.fetchMarketPrices(any()) } returns Single.just(fetchedPrices)

        //when
        presenter.register(view)

        //then
        val slot = CapturingSlot<List<Entry>>()
        verifyOrder {
            view.showLoader()
            view.showCurrentPrice(priceInTime3.price)
            view.showPriceChart(capture(slot), timeSpan)
        }

        slot.captured.size `should equal` fetchedPrices.pricesInTime.size
    }

    @Test
    fun `GIVEN retry clicked AND fetch prices error WHEN register THEN show progress then show error`() {
        //given
        every { view.onRetryClicked() } returns Observable.just(Unit)
        every { repository.fetchMarketPrices(any()) } returns Single.error(Throwable())
        every { repository.lastTimeSpan } returns TimeSpan.DAYS_60

        //when
        presenter.register(view)

        //then
        verifyOrder {
            view.showLoader()
            view.showError()
        }
    }

    @Test
    fun `GIVEN retry clicked AND fetch prices success WHEN register THEN show progress then show current price and price chart`() {
        //given
        val priceInTime1 = MarketPriceInTime(991, 0.1)
        val priceInTime2 = MarketPriceInTime(124, 12.0)
        val timeSpan = TimeSpan.ONE_YEAR

        val fetchedPrices = PricesWithTimeSpan(timeSpan, listOf(priceInTime1, priceInTime2))
        every { view.onRetryClicked() } returns Observable.just(Unit)
        every { repository.fetchMarketPrices(any()) } returns Single.just(fetchedPrices)
        every { repository.lastTimeSpan } returns timeSpan

        //when
        presenter.register(view)

        //then
        val slot = CapturingSlot<List<Entry>>()
        verifyOrder {
            view.showLoader()
            view.showCurrentPrice(priceInTime2.price)
            view.showPriceChart(capture(slot), timeSpan)
        }

        slot.captured.size `should equal` fetchedPrices.pricesInTime.size
    }
}
package com.nekodev.pricechart.data

import com.nekodev.base.data.EssentialParamMissingException
import com.nekodev.pricechart.data.dao.MarketPricesResponseRaw
import com.nekodev.pricechart.data.dao.PriceAndTimestampRaw
import org.amshove.kluent.`should equal`
import org.junit.Test

class MarketPriceResponseMapperTest {

    private val mapper = MarketPriceResponseMapper()

    @Test(expected = EssentialParamMissingException::class)
    fun `GIVEN values param null WHEN apply THEN throws EssentialParamMissingException`() {
        mapper.apply(MarketPricesResponseRaw(null))
    }

    @Test(expected = EssentialParamMissingException::class)
    fun `GIVEN empty values list WHEN apply THEN throws EssentialParamMissingException`() {
        mapper.apply(MarketPricesResponseRaw(emptyList()))
    }

    @Test
    fun `GIVEN not empty values WHEN apply THEN returns mapped values`() {
        //given
        val values = listOf(
                PriceAndTimestampRaw(12, 1.2),
                PriceAndTimestampRaw(2, 7.2)
        )

        //when
        val result = mapper.apply(MarketPricesResponseRaw(values))

        //then
        result[0].apply {
            timestampInSeconds `should equal` 12
            price `should equal` 1.2
        }
        result[1].apply {
            timestampInSeconds `should equal` 2
            price `should equal` 7.2
        }
    }
}
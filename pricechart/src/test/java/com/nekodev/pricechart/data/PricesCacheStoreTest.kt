package com.nekodev.pricechart.data

import com.nekodev.pricechart.data.dao.MarketPriceInTime
import io.mockk.mockk
import org.junit.Before
import org.junit.Test

class PricesCacheStoreTest {

    private lateinit var cache: PricesCacheStore

    @Before
    fun setUp() {
        cache = PricesCacheStore()
    }

    @Test
    fun `GIVEN price for timeSpan not saved WHEN getSavedPrices THEN single with error`() {
        //when
        val testObserver = cache.getSavedPrices(TimeSpan.ONE_YEAR).test()

        //then
        testObserver.assertError(NoSuchElementException::class.java)
    }

    @Test
    fun `GIVEN price for timeSpan saved WHEN getSavedPrices THEN single with prices`() {
        //given
        val timeSpan = TimeSpan.TWO_YEARS
        val prices = mockk<List<MarketPriceInTime>>()
        cache.putFetchedPrices(timeSpan, prices)

        //when
        val testObserver = cache.getSavedPrices(timeSpan).test()

        //then
        testObserver.assertValue(prices)
    }
}
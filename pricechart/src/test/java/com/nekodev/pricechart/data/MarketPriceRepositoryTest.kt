package com.nekodev.pricechart.data

import com.nekodev.pricechart.data.dao.MarketPriceInTime
import com.nekodev.pricechart.data.dao.MarketPricesResponseRaw
import com.nekodev.pricechart.data.dao.PricesWithTimeSpan
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.impl.annotations.MockK
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Single
import org.amshove.kluent.`should be instance of`
import org.amshove.kluent.`should be`
import org.junit.Before
import org.junit.Test

class MarketPriceRepositoryTest {

    @MockK
    lateinit var service: MarketPriceService

    @MockK
    lateinit var mapper: MarketPriceResponseMapper

    @MockK
    lateinit var cache: PricesCacheStore

    @InjectMockKs
    lateinit var repository: MarketPriceRepository

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxUnitFun = true)
    }

    @Test
    fun `GIVEN prices not in cache AND prices fetch error WHEN fetchMarketPrices THEN single with error from API`() {
        //given
        val expectedThrowable = Throwable()
        every { cache.getSavedPrices(any()) } returns Single.error(IllegalArgumentException())
        every { service.getMarketPrices(any()) } returns Single.error(expectedThrowable)

        //when
        val testObserver = repository.fetchMarketPrices(TimeSpan.DAYS_30).test()

        //then
        testObserver.assertError(expectedThrowable)
    }

    @Test
    fun `GIVEN prices not in cache AND prices fetched from api WHEN fetchMarketPrices THEN mapped market prices in single`() {
        //given
        val mockedResponse = mockk<MarketPricesResponseRaw>()
        val expectedMappedResponse = mockk<List<MarketPriceInTime>>()
        val passedTimespan = TimeSpan.DAYS_60

        every { cache.getSavedPrices(any()) } returns Single.error(IllegalArgumentException())
        every { service.getMarketPrices(any()) } returns Single.just(mockedResponse)
        every { mapper.apply(mockedResponse) } returns expectedMappedResponse

        //when
        val testObserver = repository.fetchMarketPrices(passedTimespan).test()

        //then
        testObserver.assertValue {
            it `should be instance of` PricesWithTimeSpan::class
            (it as PricesWithTimeSpan).apply {
                timeSpan `should be` passedTimespan
                pricesInTime `should be` expectedMappedResponse
            }
            true
        }
        verify {
            service.getMarketPrices(passedTimespan.label)
        }
    }

    @Test
    fun `GIVEN prices in cache WHEN fetchMarketPrices THEN mapped market prices from cache in single`() {
        //given
        val expectedMappedResponse = mockk<List<MarketPriceInTime>>()
        val passedTimespan = TimeSpan.DAYS_60

        every { cache.getSavedPrices(any()) } returns Single.just(expectedMappedResponse)

        //when
        val testObserver = repository.fetchMarketPrices(passedTimespan).test()

        //then
        testObserver.assertValue {
            it `should be instance of` PricesWithTimeSpan::class
            (it as PricesWithTimeSpan).apply {
                timeSpan `should be` passedTimespan
                pricesInTime `should be` expectedMappedResponse
            }
            true
        }
        verify {
            cache.getSavedPrices(passedTimespan)
        }
    }
}
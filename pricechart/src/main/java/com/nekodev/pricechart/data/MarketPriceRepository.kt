package com.nekodev.pricechart.data

import com.nekodev.pricechart.data.dao.FetchPriceResult
import com.nekodev.pricechart.data.dao.PricesWithTimeSpan
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MarketPriceRepository @Inject constructor(
        private val apiService: MarketPriceService,
        private val mapper: MarketPriceResponseMapper,
        private val cacheStore: PricesCacheStore) {

    var lastTimeSpan: TimeSpan = TimeSpan.DAYS_30

    fun fetchMarketPrices(timeSpan: TimeSpan): Single<FetchPriceResult> {
        lastTimeSpan = timeSpan
        return cacheStore
                .getSavedPrices(timeSpan)
                .onErrorResumeNext {
                    apiService
                            .getMarketPrices(timeSpan.label)
                            .map(mapper)
                            .doOnSuccess {
                                prices -> cacheStore.putFetchedPrices(timeSpan, prices)
                            }
                }
                .map { PricesWithTimeSpan(timeSpan, it) }
    }
}
package com.nekodev.pricechart.data.dao

data class MarketPricesResponseRaw(
        val values: List<PriceAndTimestampRaw>?
)

data class PriceAndTimestampRaw(
        val x: Long,
        val y: Double
)
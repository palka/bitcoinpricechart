package com.nekodev.pricechart.data

enum class TimeSpan(val label: String) {
    DAYS_30("30days"),
    DAYS_60("60days"),
    DAYS_180("180days"),
    ONE_YEAR("1year"),
    TWO_YEARS("2years"),
    ALL_TIME("all");
}
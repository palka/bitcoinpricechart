package com.nekodev.pricechart.data

import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
class MarketPriceModule {

    @Provides
    fun provideMarketPriceService(retrofit: Retrofit): MarketPriceService {
        return retrofit.create(MarketPriceService::class.java)
    }
}
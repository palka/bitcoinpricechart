package com.nekodev.pricechart.data

import com.github.mikephil.charting.data.Entry
import com.nekodev.pricechart.data.dao.MarketPriceInTime
import javax.inject.Inject

class PriceToEntryMapper @Inject constructor() : (MarketPriceInTime) -> Entry {

    override fun invoke(price: MarketPriceInTime): Entry {
        return Entry((price.timestampInSeconds * 1_000).toFloat(), price.price.toFloat())
    }
}
package com.nekodev.pricechart.data

import com.nekodev.base.data.EssentialParamMissingException
import com.nekodev.pricechart.data.dao.MarketPriceInTime
import com.nekodev.pricechart.data.dao.MarketPricesResponseRaw
import io.reactivex.functions.Function
import javax.inject.Inject

class MarketPriceResponseMapper @Inject constructor() : Function<MarketPricesResponseRaw, List<MarketPriceInTime>> {

    override fun apply(raw: MarketPricesResponseRaw): List<MarketPriceInTime> {
        assertEssentialParams(raw)

        return raw.values!!.map {
            MarketPriceInTime(it.x, it.y)
        }
    }

    private fun assertEssentialParams(raw: MarketPricesResponseRaw) {
        if (raw.values == null || raw.values.isEmpty()) {
            throw EssentialParamMissingException("values", raw)
        }
    }
}
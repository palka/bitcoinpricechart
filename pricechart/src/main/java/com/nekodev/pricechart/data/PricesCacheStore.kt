package com.nekodev.pricechart.data

import com.nekodev.pricechart.data.dao.MarketPriceInTime
import io.reactivex.Single
import javax.inject.Inject


class PricesCacheStore @Inject constructor() {

    private val cache: MutableMap<TimeSpan, List<MarketPriceInTime>> = mutableMapOf()

    fun putFetchedPrices(timeSpan: TimeSpan, prices: List<MarketPriceInTime>) {
        cache[timeSpan] = prices
    }

    fun getSavedPrices(timeSpan: TimeSpan): Single<List<MarketPriceInTime>> {
        return if (cache.contains(timeSpan)) {
            Single.just(cache[timeSpan])
        } else {
            Single.error(NoSuchElementException("prices for timeSpan $timeSpan not found"))
        }
    }
}
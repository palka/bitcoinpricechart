package com.nekodev.pricechart.data.dao

import com.nekodev.pricechart.data.TimeSpan

data class PricesWithTimeSpan(
        val timeSpan: TimeSpan,
        val pricesInTime: List<MarketPriceInTime>
) : FetchPriceResult
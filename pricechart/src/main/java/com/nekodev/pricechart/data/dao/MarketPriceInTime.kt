package com.nekodev.pricechart.data.dao

data class MarketPriceInTime(
        val timestampInSeconds: Long,
        val price: Double
)
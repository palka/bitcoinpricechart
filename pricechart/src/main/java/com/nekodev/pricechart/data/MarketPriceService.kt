package com.nekodev.pricechart.data

import com.nekodev.pricechart.data.dao.MarketPricesResponseRaw
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface MarketPriceService {

    @GET("market-price")
    fun getMarketPrices(@Query("timespan") timeSpanLabel: String): Single<MarketPricesResponseRaw>
}
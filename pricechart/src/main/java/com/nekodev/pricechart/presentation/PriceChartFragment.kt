package com.nekodev.pricechart.presentation

import android.graphics.Color
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.widget.TextView
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.jakewharton.rxbinding2.view.RxView
import com.nekodev.base.presentation.BaseInjectingFragment
import com.nekodev.base.util.extensions.hide
import com.nekodev.base.util.extensions.show
import com.nekodev.pricechart.R
import com.nekodev.pricechart.data.TimeSpan
import com.nekodev.pricechart.presentation.chart.DataSetPreparer
import com.nekodev.pricechart.presentation.chart.PriceChartStylist
import com.nekodev.pricechart.presentation.chart.axis.DateAxisFormatter
import com.nekodev.pricechart.presentation.chart.axis.DateFormatterFactory
import com.nekodev.pricechart.utils.extensions.getActivityComponent
import io.reactivex.Observable
import kotlinx.android.synthetic.main.fragment_price_chart.*
import javax.inject.Inject

class PriceChartFragment : BaseInjectingFragment(), PriceChartView {

    companion object {
        private const val CHART_ANIMATION_DURATION_MS = 1200
    }

    @Inject
    internal lateinit var presenter: PriceChartPresenter

    @Inject
    internal lateinit var dataSetPreparer: DataSetPreparer

    @Inject
    lateinit var priceChartStylist: PriceChartStylist

    @Inject
    lateinit var dateFormatterFactory: DateFormatterFactory

    override val layoutId = R.layout.fragment_price_chart

    override fun onInject() {
        (getActivityComponent() as PriceChartComponent.PriceChartComponentCreator)
                .createPriceChartComponent()
                .inject(this)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        presenter.register(this)

        priceChartStylist.stylePriceChart(priceLineChart)
    }

    override fun onPricesRequested(): Observable<TimeSpan> {
        return Observable.merge(
                listOf(
                        RxView.clicks(days30Button).map { TimeSpan.DAYS_30 },
                        RxView.clicks(days60Button).map { TimeSpan.DAYS_60 },
                        RxView.clicks(days180Button).map { TimeSpan.DAYS_180 },
                        RxView.clicks(yearButton).map { TimeSpan.ONE_YEAR },
                        RxView.clicks(twoYearsButton).map { TimeSpan.TWO_YEARS },
                        RxView.clicks(allTimeButton).map { TimeSpan.ALL_TIME }
                )
        )
    }

    override fun setButtonBackgrounds(timeSpan: TimeSpan) {
        setPillBackgroundIfShould(days30Button) { timeSpan == TimeSpan.DAYS_30 }
        setPillBackgroundIfShould(days60Button) { timeSpan == TimeSpan.DAYS_60 }
        setPillBackgroundIfShould(days180Button) { timeSpan == TimeSpan.DAYS_180 }
        setPillBackgroundIfShould(yearButton) { timeSpan == TimeSpan.ONE_YEAR }
        setPillBackgroundIfShould(twoYearsButton) { timeSpan == TimeSpan.TWO_YEARS }
        setPillBackgroundIfShould(allTimeButton) { timeSpan == TimeSpan.ALL_TIME }
    }

    private fun setPillBackgroundIfShould(view: TextView, isSelected: () -> Boolean) {
        view.apply {
            if (isSelected()) {
                isEnabled = false
                setBackgroundResource(R.drawable.time_active_button)
                setTextColor(Color.WHITE)
            } else {
                isEnabled = true
                setBackgroundResource(R.drawable.time_inactive_button)
                setTextColor(ContextCompat.getColor(requireContext(), R.color.default_text_color_dark))
            }
        }
    }

    override fun onRetryClicked(): Observable<Unit> {
        return RxView.clicks(errorView).map { Unit }.startWith(Unit)
    }

    override fun showLoader() {
        progressView.show()
        errorView.hide()
        priceLineChart.hide()
    }

    override fun showError() {
        errorView.show()
        progressView.hide()
        priceLineChart.hide()
    }

    override fun showCurrentPrice(currentPrice: Double) {
        currentPriceText.text = getString(R.string.current_price_format, currentPrice)
    }

    override fun showPriceChart(priceEntries: List<Entry>, timeSpan: TimeSpan) {
        progressView.hide()
        errorView.hide()

        priceLineChart.apply {
            xAxis.valueFormatter = DateAxisFormatter(dateFormatterFactory.createDateFormatterForTimeSpan(timeSpan))
            data = LineData(dataSetPreparer.prepareDataSet(priceEntries))

            invalidate()
            show()
            animateX(CHART_ANIMATION_DURATION_MS)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.unregister()
    }
}
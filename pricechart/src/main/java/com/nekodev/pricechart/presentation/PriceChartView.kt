package com.nekodev.pricechart.presentation

import com.github.mikephil.charting.data.Entry
import com.nekodev.base.presentation.BasePresenterView
import com.nekodev.pricechart.data.TimeSpan
import io.reactivex.Observable

interface PriceChartView : BasePresenterView {

    fun onPricesRequested(): Observable<TimeSpan>

    fun onRetryClicked(): Observable<Unit>

    fun showError()

    fun showLoader()

    fun showCurrentPrice(currentPrice: Double)

    fun showPriceChart(priceEntries: List<Entry>, timeSpan: TimeSpan)

    fun setButtonBackgrounds(timeSpan: TimeSpan)
}
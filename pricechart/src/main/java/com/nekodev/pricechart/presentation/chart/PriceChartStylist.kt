package com.nekodev.pricechart.presentation.chart

import android.graphics.Typeface
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.nekodev.pricechart.presentation.chart.axis.PriceAxisFormatter
import javax.inject.Inject

class PriceChartStylist @Inject constructor(
        private val priceAxisFormatter: PriceAxisFormatter,
        private val axisTextFont: Typeface
) {

    companion object {
        private const val GRID_LINE_LENGTH = 8.0f
        private const val GRID_SPACE_LENGTH = 25.0f
        private const val GRID_PHASE = 0.0f
    }

    fun stylePriceChart(priceLineChart: LineChart) {
        priceLineChart.apply {
            description.isEnabled = false
            setDrawGridBackground(false)
            axisRight.isEnabled = false
            legend.isEnabled = false

            styleAxis(xAxis)
            styleAxis(axisLeft)

            xAxis.position = XAxis.XAxisPosition.TOP_INSIDE
            axisLeft.setPosition(YAxis.YAxisLabelPosition.INSIDE_CHART)
            axisLeft.valueFormatter = priceAxisFormatter
        }
    }

    private fun styleAxis(axis: AxisBase) {
        axis.apply {
            setDrawAxisLine(false)
            typeface = axisTextFont
            enableGridDashedLine(GRID_LINE_LENGTH, GRID_SPACE_LENGTH, GRID_PHASE)
        }
    }
}
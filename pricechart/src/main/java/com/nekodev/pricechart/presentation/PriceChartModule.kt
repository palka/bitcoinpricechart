package com.nekodev.pricechart.presentation

import android.app.Application
import android.graphics.Typeface
import android.support.v4.content.ContextCompat
import android.support.v4.content.res.ResourcesCompat
import com.nekodev.pricechart.R
import com.nekodev.pricechart.presentation.chart.LineColor
import com.nekodev.pricechart.presentation.chart.axis.PriceAxisFormatter
import dagger.Module
import dagger.Provides
import dagger.Reusable

@Module
class PriceChartModule {

    @Provides
    fun provideLineColor(application: Application): LineColor {
        return ContextCompat.getColor(application, R.color.color_bitcoin_accent)
    }

    @Provides
    fun providePriceAxisFormatter(application: Application): PriceAxisFormatter {
        return PriceAxisFormatter(application.resources)
    }

    @Provides
    @Reusable
    fun provideMediumTypeface(application: Application): Typeface {
        return ResourcesCompat.getFont(application, R.font.google_sans_medium)!!
    }
}
package com.nekodev.pricechart.presentation.chart

import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineDataSet
import javax.inject.Inject

typealias LineColor = Int

class DataSetPreparer @Inject constructor(private val lineColor: LineColor) {

    companion object {
        private const val LINE_WIDTH = 3f
        private const val EMPTY_LABEL = ""
    }

    fun prepareDataSet(entries: List<Entry>): LineDataSet {
        return LineDataSet(entries, EMPTY_LABEL).apply {
            setDrawHighlightIndicators(false)
            setDrawValues(false)
            setDrawCircles(false)
            lineWidth = LINE_WIDTH
            color = lineColor
        }
    }
}
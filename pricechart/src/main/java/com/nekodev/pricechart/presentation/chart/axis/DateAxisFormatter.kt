package com.nekodev.pricechart.presentation.chart.axis

import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.formatter.IAxisValueFormatter
import java.text.SimpleDateFormat
import kotlin.math.roundToLong

class DateAxisFormatter(private val dateFormatter: SimpleDateFormat) : IAxisValueFormatter {

    override fun getFormattedValue(value: Float, axis: AxisBase?): String {
        return dateFormatter.format(value.roundToLong())
    }

}
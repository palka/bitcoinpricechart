package com.nekodev.pricechart.presentation.chart.axis

import com.nekodev.pricechart.data.TimeSpan
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class DateFormatterFactory @Inject constructor() {

    companion object {
        private const val DAY_AND_MONTH = "dd MMM"
        private const val MONTH = "MMM"
        private const val MONTH_AND_YEAR = "MMM YYYY"
        private const val YEAR = "YYYY"
    }

    fun createDateFormatterForTimeSpan(timeSpan: TimeSpan): SimpleDateFormat {
        return SimpleDateFormat(getDateFormat(timeSpan), Locale.getDefault())
    }

    private fun getDateFormat(timeSpan: TimeSpan): String {
        return when (timeSpan) {
            TimeSpan.DAYS_30 -> DAY_AND_MONTH
            TimeSpan.DAYS_60 -> DAY_AND_MONTH
            TimeSpan.DAYS_180 -> MONTH
            TimeSpan.ONE_YEAR -> MONTH
            TimeSpan.TWO_YEARS -> MONTH_AND_YEAR
            TimeSpan.ALL_TIME -> YEAR
        }
    }
}
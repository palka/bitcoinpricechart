package com.nekodev.pricechart.presentation.chart.axis

import android.content.res.Resources
import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.formatter.IAxisValueFormatter
import com.nekodev.pricechart.R
import kotlin.math.roundToInt

class PriceAxisFormatter(private val resources: Resources) : IAxisValueFormatter {

    override fun getFormattedValue(value: Float, axis: AxisBase?): String {
        return resources.getString(R.string.axis_price_format, value.roundToInt())
    }
}
package com.nekodev.pricechart.presentation

import com.nekodev.base.presentation.BasePresenter
import com.nekodev.base.util.rx.BaseSchedulerProvider
import com.nekodev.pricechart.data.MarketPriceRepository
import com.nekodev.pricechart.data.PriceToEntryMapper
import com.nekodev.pricechart.data.TimeSpan
import com.nekodev.pricechart.data.dao.FetchPriceError
import com.nekodev.pricechart.data.dao.FetchPriceResult
import com.nekodev.pricechart.data.dao.PricesWithTimeSpan
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject

class PriceChartPresenter @Inject constructor(
        private val schedulers: BaseSchedulerProvider,
        private val priceRepository: MarketPriceRepository,
        private val priceToEntryMapper: PriceToEntryMapper
) : BasePresenter<PriceChartView>() {

    override fun register(view: PriceChartView) {
        super.register(view)
        addToUnsubscribe(createFetchMarketPriceDisposable(view))
    }

    private fun createFetchMarketPriceDisposable(view: PriceChartView): Disposable {
        return Observable.merge(
                view.onPricesRequested(),
                view.onRetryClicked().map { priceRepository.lastTimeSpan })
                .doOnNext {
                    view.showLoader()
                    view.setButtonBackgrounds(it)
                }
                .switchMapSingle { fetchMarketPrices(it) }
                .observeOn(schedulers.ui())
                .subscribeBy(
                        onNext = { result ->
                            (result as? PricesWithTimeSpan)?.let {
                                view.showFetchedData(it)
                            } ?: run {
                                view.showError()
                            }
                        },
                        onError = {
                            view.showError()
                        }
                )
    }

    private fun PriceChartView.showFetchedData(prices: PricesWithTimeSpan) {
        prices.apply {
            showCurrentPrice(pricesInTime.last().price)
            showPriceChart(pricesInTime.map(priceToEntryMapper), timeSpan)
        }
    }

    private fun fetchMarketPrices(timeSpan: TimeSpan): Single<out FetchPriceResult> {
        return priceRepository.fetchMarketPrices(timeSpan)
                .subscribeOn(schedulers.io())
                .onErrorReturnItem(FetchPriceError())
    }
}

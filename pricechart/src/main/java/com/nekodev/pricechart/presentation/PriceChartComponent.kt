package com.nekodev.pricechart.presentation

import com.nekodev.base.injection.scopes.FragmentScope
import dagger.Subcomponent


@FragmentScope
@Subcomponent(
        modules = [
            PriceChartModule::class
        ]
)
interface PriceChartComponent {

    fun inject(fragment: PriceChartFragment)

    interface PriceChartComponentCreator {

        fun createPriceChartComponent(): PriceChartComponent
    }
}
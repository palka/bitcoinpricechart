package com.nekodev.pricechart.utils.extensions

import android.support.v4.app.Fragment
import com.nekodev.base.presentation.BaseInjectingActivity

fun Fragment.getActivityComponent() : Any {
    return (activity as BaseInjectingActivity<*>).getComponent()
}
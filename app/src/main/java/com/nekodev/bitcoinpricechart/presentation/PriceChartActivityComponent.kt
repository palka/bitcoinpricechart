package com.nekodev.bitcoinpricechart.presentation

import com.nekodev.base.injection.scopes.ActivityScope
import com.nekodev.pricechart.presentation.PriceChartComponent
import dagger.Subcomponent

@ActivityScope
@Subcomponent
interface PriceChartActivityComponent : PriceChartComponent.PriceChartComponentCreator {

    fun inject(priceChartActivity: PriceChartActivity)
}
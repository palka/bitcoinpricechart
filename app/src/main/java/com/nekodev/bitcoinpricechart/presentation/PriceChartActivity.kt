package com.nekodev.bitcoinpricechart.presentation

import android.os.Bundle
import com.nekodev.base.presentation.BaseInjectingActivity
import com.nekodev.base.util.extensions.replaceFragmentIfNotExistsAlready
import com.nekodev.bitcoinpricechart.R
import com.nekodev.bitcoinpricechart.utils.extensions.getAppComponent
import com.nekodev.pricechart.presentation.PriceChartFragment

class PriceChartActivity : BaseInjectingActivity<PriceChartActivityComponent>() {

    override val layoutId = R.layout.activity_price_chart

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportFragmentManager.replaceFragmentIfNotExistsAlready(R.id.priceChartFragmentFrame) {
            PriceChartFragment()
        }
    }

    override fun onInject(component: PriceChartActivityComponent) {
        component.inject(this)
    }

    override fun createComponent(): PriceChartActivityComponent {
        return getAppComponent().getPriceChartActivityComponent()
    }
}
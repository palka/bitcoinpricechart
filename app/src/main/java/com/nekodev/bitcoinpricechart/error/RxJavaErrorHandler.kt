package com.nekodev.bitcoinpricechart.error

import android.support.annotation.VisibleForTesting
import io.reactivex.exceptions.UndeliverableException
import io.reactivex.functions.Consumer

class RxJavaErrorHandler : Consumer<Throwable> {

    @Throws(Exception::class)
    override fun accept(throwable: Throwable) {
        if (throwable is UndeliverableException) {
            //ignore or track RxJavaUndeliverableException(throwable)
        } else {
            // Expected OnErrorNotImplementedException or ProtocolViolationException or any different bug
            throw RxJavaFatalException(throwable)
        }
    }

    @VisibleForTesting
    class RxJavaFatalException(cause: Throwable) : Exception(cause)

    @Suppress("unused")
    private class RxJavaUndeliverableException(cause: Throwable) : Exception(cause)
}
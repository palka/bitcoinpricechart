package com.nekodev.bitcoinpricechart.injection.module

import com.nekodev.pricechart.data.MarketPriceModule
import dagger.Module

@Module(includes = [
    MarketPriceModule::class
])
class DataModule
package com.nekodev.bitcoinpricechart.injection.component

import android.app.Application
import com.nekodev.base.injection.modules.SchedulerModule
import com.nekodev.bitcoinpricechart.injection.module.DataModule
import com.nekodev.bitcoinpricechart.injection.module.NetworkModule
import com.nekodev.bitcoinpricechart.presentation.PriceChartActivityComponent
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [
    SchedulerModule::class,
    NetworkModule::class,
    DataModule::class
])
interface AppComponent {

    fun getPriceChartActivityComponent(): PriceChartActivityComponent

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }
}
package com.nekodev.bitcoinpricechart.utils.extensions

import android.app.Activity
import com.nekodev.bitcoinpricechart.BitcoinApp
import com.nekodev.bitcoinpricechart.injection.component.AppComponent

fun Activity.getAppComponent(): AppComponent =
        (this.application as BitcoinApp).component
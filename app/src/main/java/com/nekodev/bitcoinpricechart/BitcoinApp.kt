package com.nekodev.bitcoinpricechart

import android.app.Application
import com.nekodev.bitcoinpricechart.error.RxJavaErrorHandler
import com.nekodev.bitcoinpricechart.injection.component.AppComponent
import com.nekodev.bitcoinpricechart.injection.component.DaggerAppComponent
import io.reactivex.plugins.RxJavaPlugins

class BitcoinApp : Application() {

    lateinit var component: AppComponent
        private set

    override fun onCreate() {
        super.onCreate()
        component = DaggerAppComponent.builder()
                .application(this)
                .build()

        RxJavaPlugins.setErrorHandler(RxJavaErrorHandler())
    }
}
package com.nekodev.bitcoinpricechart

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.isDisplayed
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import com.nekodev.bitcoinpricechart.presentation.PriceChartActivity
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class PriceChartActivityTest {

    @Rule
    @JvmField
    val mainActivityRule = ActivityTestRule(PriceChartActivity::class.java, false, true)

    @Test
    fun checkAllButtonsDisplayed() {
        checkDisplayed(R.id.days30Button)
        checkDisplayed(R.id.days60Button)
        checkDisplayed(R.id.days180Button)
        checkDisplayed(R.id.yearButton)
        checkDisplayed(R.id.twoYearsButton)
        checkDisplayed(R.id.allTimeButton)
    }

    private fun checkDisplayed(id: Int) {
        onView(withId(id)).check(matches(isDisplayed()))
    }
}

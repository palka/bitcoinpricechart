package com.nekodev.bitcoinpricechart.error

import io.reactivex.exceptions.OnErrorNotImplementedException
import io.reactivex.exceptions.UndeliverableException
import org.junit.Test

class RxJavaErrorHandlerTest {

    @Test
    fun `GIVEN UndeliverableException WHEN accept THEN ignore`() {
        RxJavaErrorHandler().accept(UndeliverableException(Throwable()))
    }

    @Test(expected = RxJavaErrorHandler.RxJavaFatalException::class)
    fun `GIVEN OnErrorNotImplementedException WHEN accept THEN rethrow`() {
        RxJavaErrorHandler().accept(OnErrorNotImplementedException(Throwable()))
    }
}
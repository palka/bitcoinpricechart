package com.nekodev.base.injection.scopes

import javax.inject.Scope

@Scope
annotation class ActivityScope
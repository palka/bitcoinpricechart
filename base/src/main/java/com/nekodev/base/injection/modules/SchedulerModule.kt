package com.nekodev.base.injection.modules

import com.nekodev.base.util.rx.BaseSchedulerProvider
import com.nekodev.base.util.rx.SchedulerProvider
import dagger.Binds
import dagger.Module

@Module
interface SchedulerModule {

    @Binds
    @Suppress("unused")
    fun provideSchedulerProvider(schedulerProvider: SchedulerProvider): BaseSchedulerProvider
}
package com.nekodev.base.presentation

import android.os.Bundle

abstract class BaseInjectingActivity<Component : Any> : BaseActivity() {

    private var component: Component? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        component = createComponent().also {
            onInject(it)
        }

        super.onCreate(savedInstanceState)
    }

    fun getComponent(): Component {
        return requireNotNull(component)
    }

    protected abstract fun onInject(component: Component)

    protected abstract fun createComponent(): Component
}

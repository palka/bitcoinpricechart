package com.nekodev.base.util.extensions

import android.support.annotation.IdRes
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager

inline fun <reified T : Fragment> FragmentManager.replaceFragmentIfNotExistsAlready(
        @IdRes container: Int,
        createFragment: () -> T
) {
    if (findFragmentById(container) !is T) {
        replaceFragment(container, createFragment())
    }
}

fun FragmentManager.replaceFragment(
        @IdRes container: Int,
        fragment: Fragment
) {
    beginTransaction()
            .replace(container, fragment)
            .commit()
}